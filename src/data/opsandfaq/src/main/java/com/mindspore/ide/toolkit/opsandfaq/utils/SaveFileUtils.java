package com.mindspore.ide.toolkit.opsandfaq.utils;

import com.mindspore.ide.toolkit.opsandfaq.MdPathString;
import com.mindspore.ide.toolkit.opsandfaq.bean.DataBean;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

public class SaveFileUtils {
    /**
     * @param dataBeans
     * @return
     * @throws IOException
     */
    public static boolean saveFileCsv(List<DataBean> dataBeans) throws IOException {
        File file;
        if (MdPathString.Language.equals("cn")) {
            file = new File("D:" + "/" + "data_cn" + ".csv");
        } else {
            file = new File("D:" + "/" + "data_en" + ".csv");
        }
        //构建输出流，同时指定编码
        OutputStreamWriter ow = new OutputStreamWriter(new FileOutputStream(file), "utf-8");
        String[] titles = {"title", "content", "link", "process", "tag"};
        //csv文件是逗号分隔，除第一个外，每次写入一个单元格数据后需要输入逗号
        for (String title : titles) {
            ow.write(title);
            ow.write(",");
        }
        //写完文件头后换行
        ow.write("\r\n");
        //写内容
        for (DataBean dataBean : dataBeans) {
            ow.write(dataBean.getTitle());
            ow.write(" ");
            ow.write(",");
            ow.write(dataBean.getContent());
            ow.write(" ");
            ow.write(",");
            ow.write(dataBean.getLink());
            ow.write(" ");
            ow.write(",");
            ow.write(dataBean.getProcess());
            ow.write(" ");
            ow.write(",");
            ow.write(dataBean.getTag());
            ow.write(" ");
            ow.write(",");
            //写完一行换行
            ow.write("\r\n");
        }
        ow.flush();
        ow.close();
        return true;
    }
}