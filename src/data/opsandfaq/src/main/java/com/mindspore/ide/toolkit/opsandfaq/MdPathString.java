package com.mindspore.ide.toolkit.opsandfaq;

import com.google.gson.JsonParseException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

public class MdPathString {
    public static final String Language = "en";

    public static final String PYTORCH_MD_STR = readMdFile(
            MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "pytorch_api_mapping.md");

    public static final String TENSORFLOW_MD_STR = readMdFile(
            MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "tensorflow_api_mapping.md");

    public static final String MD_1 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "data_processing.md");

    public static final String MD_2 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "distributed_configure.md");

    public static final String MD_3 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "feature_advice.md");

    public static final String MD_4 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "implement_problem.md");

    public static final String MD_5 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "inference.md");

    public static final String MD_6 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "installation.md");

    public static final String MD_7 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "network_compilation.md");

    public static final String MD_8 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "operators_compile.md");

    public static final String MD_9 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "performance_tuning.md");

    public static final String MD_10 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "precision_tuning.md");

    public static final String MD_11 = readMdFile(MdPathString.SLASH + "testdata" + MdPathString.SLASH + Language
            + MdPathString.SLASH + "usage_migrate_3rd.md");

    private static final String SLASH = "/";

    private static String readMdFile(String mdPath) {
        StringBuilder stringBuilder = new StringBuilder();
        try (InputStream inputStream = MdPathString.class.getResourceAsStream(mdPath);
             InputStreamReader fileReader = new InputStreamReader(inputStream, "UTF-8");
             BufferedReader bufferedReader = new BufferedReader(fileReader)) {
            boolean isFirstLine = true;
            String line = null;
            while ((line = bufferedReader.readLine()) != null) {
                if (!isFirstLine) {
                    stringBuilder.append(System.getProperty("line.separator"));
                } else {
                    isFirstLine = false;
                }
                stringBuilder.append(line);
            }
        } catch (IOException | JsonParseException exception) {
            exception.printStackTrace();
        }
        return stringBuilder.toString();
    }
}