package com.mindspore.ide.toolkit.opsandfaq;

import com.mindspore.ide.toolkit.opsandfaq.bean.DataBean;
import com.mindspore.ide.toolkit.opsandfaq.utils.ApiMappingUtils;
import com.mindspore.ide.toolkit.opsandfaq.utils.FaqUtils;
import com.mindspore.ide.toolkit.opsandfaq.utils.SaveFileUtils;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class NewData {
    public static void main(String[] args) {
        List<DataBean> dataBeans = new LinkedList<>();
        // 处理api映射数据
        testApi(dataBeans);
        // 处理FAQ数据
        testFaq(dataBeans);
        // 保存数据到文件
        try {
            boolean isBoolean = SaveFileUtils.saveFileCsv(dataBeans);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 处理api映射数据
     *
     * @param dataBeans list
     */
    private static void testApi(List<DataBean> dataBeans) {
        // pytorch数据
        List<DataBean> dataBean1 = ApiMappingUtils.apiMdStringToList(MdPathString.PYTORCH_MD_STR,
                "operator",
                "算子",
                "pytorch|api mapping",
                "pytorch|api映射"
        );
        // tensorflow数据
        List<DataBean> dataBean2 = ApiMappingUtils.apiMdStringToList(MdPathString.TENSORFLOW_MD_STR,
                "operator",
                "算子",
                "tensorflow|api mapping",
                "tensorflow|api映射"
        );
        dataBeans.addAll(dataBean1);
        dataBeans.addAll(dataBean2);
    }

    /**
     * 处理faq数据
     *
     * @param dataBeans list
     */
    private static void testFaq(List<DataBean> dataBeans) {
        List<DataBean> dataBean1 = FaqUtils.faqMdStringToList(MdPathString.MD_1,
                "https://www.mindspore.cn/docs/en/r1.7/faq/data_processing.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/data_processing.html",
                "FAQ",
                "data_processing|FAQ",
                "数据处理|FAQ"
        );
        List<DataBean> dataBean2 = FaqUtils.faqMdStringToList(MdPathString.MD_2,
                "https://www.mindspore.cn/docs/en/r1.7/faq/distributed_configure.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/distributed_configure.html",
                "FAQ",
                "distributed_configure|FAQ",
                "分布式配置|FAQ"
        );
        List<DataBean> dataBean3 = FaqUtils.faqMdStringToList(MdPathString.MD_3,
                "https://www.mindspore.cn/docs/en/r1.7/faq/feature_advice.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/feature_advice.html",
                "FAQ",
                "feature_advice|FAQ",
                "特性咨询|FAQ"
        );
        List<DataBean> dataBean4 = FaqUtils.faqMdStringToList(MdPathString.MD_4,
                "https://www.mindspore.cn/docs/en/r1.7/faq/implement_problem.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/implement_problem.html",
                "FAQ",
                "implement_problem|FAQ",
                "执行问题|FAQ"
        );
        List<DataBean> dataBean5 = FaqUtils.faqMdStringToList(MdPathString.MD_5,
                "https://www.mindspore.cn/docs/en/r1.7/faq/inference.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/inference.html",
                "FAQ",
                "inference|FAQ",
                "推理|FAQ"
        );
        List<DataBean> dataBean6 = FaqUtils.faqMdStringToList(MdPathString.MD_6,
                "https://www.mindspore.cn/docs/en/r1.7/faq/installation.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/installation.html",
                "FAQ",
                "installation|FAQ",
                "安装|FAQ"
        );
        List<DataBean> dataBean7 = FaqUtils.faqMdStringToList(MdPathString.MD_7,
                "https://www.mindspore.cn/docs/en/r1.7/faq/network_compilation.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/network_compilation.html",
                "FAQ",
                "network_compilation|FAQ",
                "网络编译|FAQ"
        );
        List<DataBean> dataBean8 = FaqUtils.faqMdStringToList(MdPathString.MD_8,
                "https://www.mindspore.cn/docs/en/r1.7/faq/operators_compile.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/operators_compile.html",
                "FAQ",
                "operators_compile|FAQ",
                "算子编译|FAQ"
        );
        List<DataBean> dataBean9 = FaqUtils.faqMdStringToList(MdPathString.MD_9,
                "https://www.mindspore.cn/docs/en/r1.7/faq/performance_tuning.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/performance_tuning.html",
                "FAQ",
                "performance_tuning|FAQ",
                "性能调优|FAQ"
        );
        List<DataBean> dataBean10 = FaqUtils.faqMdStringToList(MdPathString.MD_10,
                "https://www.mindspore.cn/docs/en/r1.7/faq/precision_tuning.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/precision_tuning.html",
                "FAQ",
                "precision_tuning|FAQ",
                "精度调优|FAQ"
        );
        List<DataBean> dataBean11 = FaqUtils.faqMdStringToList(MdPathString.MD_11,
                "https://www.mindspore.cn/docs/en/r1.7/faq/usage_migrate_3rd.html",
                "https://www.mindspore.cn/docs/zh-CN/r1.7/faq/usage_migrate_3rd.html",
                "FAQ",
                "usage_migrate_3rd|FAQ",
                "第三方框架迁移使用|FAQ"
        );
        dataBeans.addAll(dataBean1);
        dataBeans.addAll(dataBean2);
        dataBeans.addAll(dataBean3);
        dataBeans.addAll(dataBean4);
        dataBeans.addAll(dataBean5);
        dataBeans.addAll(dataBean6);
        dataBeans.addAll(dataBean7);
        dataBeans.addAll(dataBean8);
        dataBeans.addAll(dataBean9);
        dataBeans.addAll(dataBean10);
        dataBeans.addAll(dataBean11);
    }
}