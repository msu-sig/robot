package com.mindspore.ide.toolkit.opsandfaq.utils;

import com.mindspore.ide.toolkit.opsandfaq.MdPathString;
import com.mindspore.ide.toolkit.opsandfaq.bean.DataBean;
import com.vladsch.flexmark.ast.Link;
import com.vladsch.flexmark.ast.Paragraph;
import com.vladsch.flexmark.ast.SoftLineBreak;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class ApiMappingUtils {
    /**
     * 将api映射数据转换为list
     *
     * @param mdString  api映射数据
     * @param processEn 英文流程
     * @param processCn 中文流程
     * @param tagEn     英文标签
     * @param tagCn     中文标签
     * @return list
     */
    public static List<DataBean> apiMdStringToList(String mdString, String processEn,
                                                   String processCn, String tagEn, String tagCn) {
        List<DataBean> dataBeanList = new LinkedList<>();
        MutableDataSet options = new MutableDataSet();
        Parser parser = Parser.builder(options).build();
        Node document = parser.parse(mdString);
        document.getChildIterator().forEachRemaining(paragraphNode -> {
            if (paragraphNode instanceof Paragraph) {
                LinkedHashMap<String, String> mapList = new LinkedHashMap<>();
                paragraphNode.getChildIterator().forEachRemaining(innerNode -> {
                    if (innerNode instanceof Link) {
                        Link link = (Link) innerNode;
                        String text = link.getText().toString();
                        String url = link.getUrl().toString();
                        mapList.put(text, url);
                    } else if (innerNode instanceof SoftLineBreak) {
                        // 换行处理缓存数据
                        if (MdPathString.Language.equals("cn")) {
                            setMapData(mapList, dataBeanList, processCn, tagCn);
                        } else {
                            setMapData(mapList, dataBeanList, processEn, tagEn);
                        }
                    } else {
                        innerNode.getNodeName();
                    }
                });
                // 防止一条数据没有换行直接进入下一个循环导致数据丢失
                if (MdPathString.Language.equals("cn")) {
                    setMapData(mapList, dataBeanList, processCn, tagCn);
                } else {
                    setMapData(mapList, dataBeanList, processEn, tagEn);
                }
            }
        });
        return dataBeanList;
    }

    /**
     * 处理数据
     *
     * @param mapList          map list
     * @param dataBeanList list
     * @param process          流程
     * @param tag              标签
     */
    private static void setMapData(LinkedHashMap<String, String> mapList,
                                   List<DataBean> dataBeanList, String process, String tag) {
        if (mapList.size() <= 1) {
            return;
        }
        Map.Entry<String, String>[] entries = mapList.entrySet().toArray(new Map.Entry[]{});
        // 获取map最后一条数据
        Map.Entry<String, String> entry = entries[entries.length - 1];
        // map最后一条数据是diff或者差异对比
        if (entry.getKey().equals("差异对比") || entry.getKey().equals("diff")) {
            // 如果最后一条是差异对比或者diff，有用数据长度减一
            processSpecialData(entries, entries.length - 1, dataBeanList, process, tag);
        } else {
            processSpecialData(entries, entries.length, dataBeanList, process, tag);
        }
        // 清除缓存数据
        mapList.clear();
    }

    /**
     * 处理数据
     *
     * @param entries          数据
     * @param mapLength        有用的数据长度
     * @param dataBeanList list
     * @param process          流程
     * @param tag              标签
     */
    private static void processSpecialData(Map.Entry<String, String>[] entries,
                                           int mapLength, List<DataBean> dataBeanList, String process, String tag) {
        if (mapLength <= 1) {
            // 数据缺失直接返回
            return;
        }
        if (mapLength == 2) {
            // 单条数据
            addMapData(entries, mapLength, 0, dataBeanList, process, tag);
        }
        if (mapLength > 2) {
            // 多条数据
            for (int i = 0; i < mapLength - 1; i++) {
                addMapData(entries, mapLength, i, dataBeanList, process, tag);
            }
        }
    }

    /**
     * 添加数据
     *
     * @param entries          数据
     * @param mapLength        有用的数据长度
     * @param in               其他api占用的位置
     * @param dataBeanList list
     * @param process          流程
     * @param tag              标签
     */
    private static void addMapData(Map.Entry<String, String>[] entries, int mapLength,
                                   int in, List<DataBean> dataBeanList, String process, String tag) {
        // 最后一条数据默认是MindSpore的数据
        Map.Entry<String, String> entry = entries[mapLength - 1];
        // 其他api的数据
        String otherString = entries[in].getKey();
        DataBean dataBean = new DataBean();
        dataBean.setTitle(otherString);
        dataBean.setContent(entry.getKey());
        dataBean.setLink(entry.getValue());
        dataBean.setProcess(process);
        dataBean.setTag(tag);
        dataBeanList.add(dataBean);
    }
}