package com.mindspore.ide.toolkit.opsandfaq.utils;

import com.mindspore.ide.toolkit.opsandfaq.MdPathString;
import com.mindspore.ide.toolkit.opsandfaq.bean.DataBean;
import com.vladsch.flexmark.ast.*;
import com.vladsch.flexmark.parser.Parser;
import com.vladsch.flexmark.util.ast.Node;
import com.vladsch.flexmark.util.data.MutableDataSet;

import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class FaqUtils {
    /**
     * 将faq数据转换为list
     *
     * @param mdString faq数据
     * @param linkEn   链接英文
     * @param linkCn   链接中文
     * @param process  流程
     * @param tagEn    标签英文
     * @param tagCn    标签中文
     * @return list
     */
    public static List<DataBean> faqMdStringToList(String mdString, String linkEn, String linkCn,
                                                   String process, String tagEn, String tagCn) {
        List<DataBean> dataBeanList = new LinkedList<>();
        MutableDataSet options = new MutableDataSet();
        Parser parser = Parser.builder(options).build();
        Node document = parser.parse(mdString);
        // 所有数据
        LinkedHashMap<String, String> mapList = new LinkedHashMap<>();
        // q数据
        LinkedHashMap<String, String> mapListQ = new LinkedHashMap<>();
        final boolean[] isQProcess = {false};
        document.getChildIterator().forEachRemaining(paragraphNode -> {
            String data = paragraphNode.getChars().toString();
            if (paragraphNode instanceof Paragraph) {
                if (data.startsWith("<font size=3>**Q:") || data.startsWith("<font size=3>**Q：")) {
                    // 问开始 答结束
                    isQProcess[0] = true;
                    if (MdPathString.Language.equals("cn")) {
                        addMapData(mapList, linkCn, process, tagCn, dataBeanList, mapListQ);
                    } else {
                        addMapData(mapList, linkEn, process, tagEn, dataBeanList, mapListQ);
                    }
                    mapList.clear();
                    mapListQ.clear();
                    mapListQ.put("title", data);
                    mapList.put("title", data);
                } else if (data.startsWith("A：") || data.startsWith("A:")) {
                    // 问结束 答开始
                    isQProcess[0] = false;
                    mapList.put(data, data);
                } else {
                    mapList.put(data, data);
                }
            } else if (paragraphNode instanceof OrderedList |
                    paragraphNode instanceof FencedCodeBlock |
                    paragraphNode instanceof BulletList |
                    paragraphNode instanceof BlockQuote) {
                mapList.put(data, data);
                if (isQProcess[0]) {
                    mapListQ.put(data, data);
                }
            }
        });
        if (MdPathString.Language.equals("cn")) {
            addMapData(mapList, linkCn, process, tagCn, dataBeanList, mapListQ);
        } else {
            addMapData(mapList, linkEn, process, tagEn, dataBeanList, mapListQ);
        }
        return dataBeanList;
    }

    /**
     * 添加数据
     *
     * @param mapList          map list
     * @param link             链接
     * @param process          流程
     * @param tag              标签
     * @param dataBeanList list
     * @param mapListQ         map listQ
     */
    private static void addMapData(LinkedHashMap<String, String> mapList, String link, String process,
                                   String tag, List<DataBean> dataBeanList,
                                   LinkedHashMap<String, String> mapListQ) {
        if (mapList.size() <= 1) {
            return;
        }
        if (mapListQ.size() < 1) {
            return;
        }
        Map.Entry<String, String>[] entries = mapList.entrySet().toArray(new Map.Entry[]{});
        Map.Entry<String, String>[] entriesQ = mapListQ.entrySet().toArray(new Map.Entry[]{});
        String contentQ = "";
        for (Map.Entry<String, String> stringStringEntry : entriesQ) {
            contentQ = contentQ + stringStringEntry.getValue();
        }
        String contentA = "";
        for (int i = entriesQ.length; i < entries.length; i++) {
            contentA = contentA + entries[i].getValue();
        }
        DataBean dataBean = new DataBean();
        dataBean.setTitle(contentQ.trim().replace("<font size=3>**Q: ", "")
                .replace("**</font>", "")
                .replace("<font size=3>**Q：", "")
                .replace(",", "，")
                .replace("\r\n", " ")
                .replace("\n", " ")
                .replace("\r", " ")
                .trim());
        dataBean.setContent(contentA.trim().replace("A:", "")
                .replace("A：", "")
                .replace(",", "，")
                .replace("\r\n", " ")
                .replace("\n", " ")
                .replace("\r", " ")
                .trim());
        dataBean.setLink(link);
        dataBean.setProcess(process);
        dataBean.setTag(tag);
        dataBeanList.add(dataBean);
    }
}