# 基于官网设计、语法支持、迁移指南、API四个模块的数据集

## 概述
如题所示，本项目是基于官网设计、语法支持、迁移指南、API四个模块整理的数据集，用于做问答机器人的语料库。采用python语言，直接从官网中读取模块的html文档，并对其各项属性进行划分所得到的格式为.csv的数据集。其headline一共分为如下部分：title，link，content，process和tag。

## 基本概念
每个模块的一级标题不同，如语法支持模块的一级标题分别为：静态图语法支持、Tensor索引支持。二级标题是每个一级标题下的小标题，如分别为：概述、数据类型、原型、运算符、复合语句、函数。

## Title
Title为"一级标题§二级标题"，采用的是文本的格式，去掉了一些xml标签，本身这些标签对于问题的表述没有较大影响。
当一级标题和二级标题之间有内容或者只有一级标题时，Title为"一级标题§一级标题"
## Link
Link为每个模块的一级标题的源网页地址链接。

## Content
Content为每个模块中二级标题包含的内容，与title一致，去掉了一些xml标签，同时以下几点需要注意：公式将转换成latex格式；表格从同一行左右两列变成上下两行的模式（一一对应），如语法支持模块的静态图语法支持-运算符中的表格（网址：https://www.mindspore.cn/docs/zh-CN/r1.7/note/static_graph_syntax_support.html；csv数据集:html_grammar_dataset.csv）；还去掉的部分名词自带的网页链接（多是用于展开介绍）以及图片（多是用于展示）。

## Process
每个模块的Process是其一级标题，如语法支持模块的process分别为：静态图语法支持、Tensor索引支持。其分类标准参照的是语料库的分类。

## Tag
Tag为官网设计、语法支持、迁移指南、API四个模块所涉及到的领域，没有具体的分类标准。暂时将tag设置为以下类别：'白皮书', 'MindSpore', '全场景', '自动微分', 'GradOperation', 'MindSpore', 'Parameter Server', '并行', '优化器', 'Host&Device', '分布式', '算子', '接口', '数据集', '集合通信', '同步模式', '损失函数', '控制流', '自由变量', '闭包', 'MindRecord', 'Pipeline', 'Adaptor', 'Optimizer', 'Runtime', 'Operators', 'Callback',  'MindSpore AKG', 'Auto-Tiling', 'Auto-Mapping', 'Davinci', '样例脚本', 'GPU', 'CPU', 'CuBLAS库', 'Linux', 'Ascend', '模型推理', 'MindInsight', 'plugin_name', 'MindArmour', 'AI Fuzzer', 'MINDIR', '缩略语','数据类型', '原型', '运算符', '语句', '内置函数', '网络',  'PyNative', 'MindConverter', '评估', 'TensorFlow', 'Pytorch', 'ONNX', 'PB', 'API', 'Model', 'SGD', 'loss', 'metric', 'ARM', 'AST',  'TensorBoard', 'Dataloader', 'GeneratorDataset', ' GradOperation', 'TrainOneStepCell', '反向传播', '求导', 'weight decay', 'LR', '学习率', 'Aten', '调试', 'MindOptimizer', '精度', 'checklist', '数据', 'loss scale', '超参', '计算图', '精度', '可视化', '模型结构', '优化', '迭代', 'ModelZoo', 'MindSpore Serving', '脚本', '单机', 'profiling', 'MindData', '框架', '性能', 'Dataset', 'macOS', 'SDK版本', 'SciPy', 'whl包', 'protobuf', 'Ubuntu', 'Windows', 'WSL', 'Conda', 'Serving', 'gmp', 'cuda', 'JupyterLab',  'eval', 'NLP', 'HCLL', 'ModelArts', 'Graph','C++', 'AIPP', 'OpenMPI', 'NCLL', 'RDMA', 'IB', 'RoCE', 'taichi', 'Caffe', 'NPU', 'Embedding', 'Parameter Server', 'JIT Fallback'。如果该问题不属于以上所有tag，则将其定义为’ ’，即空字符。

# Python源代码修改方法
####（1）如要增加语料库，需要在url，process两个列表中添加元素，其中，url为语料库的网址链接，process为文章块的id。要注意的是，这三个列表需要对齐，否则会发生错误。
####（2）如在原有语料库的基础上增加二级标题包含的内容，只需要重新运行该代码即可。
####（3）如要增加或者删减tag，只需要在tag列表中进行相应的添加或删减。在进行tag查找的时候，是不区分大小写的，所以进行添加时，对于大小写没有要求，最终输出是按照原文本的大小写进行输出的。
####（4）本代码可以将一级标题和二级标题中含有的特殊字符，如："::"、"_"、" "、"&"、"（"、"）"，转换成html解析器解析出的对应特殊字符，如："-"、""。如果一级标题和二级标题的特殊字符有变，需及时更改相关代码。
####（5）html解析器解析的各级标题后都有特殊字符"¶"，代码中将其用以定位，更改代码时需注意。