# 基于官网FAQ文档的数据集
## 概述
如题所示，本项目是基于官网FAQ文档整理的数据集，用于做问答机器人的语料库。采用python语言，从官网中下载FAQ文档，并对其各项属性进行划分所得到的格式为.csv的数据集。其headline一共分为如下部分：title，link，content，process和tag。

## Title
Title为FAQ中的questions，是可能出现的问题。采用的是文本的格式，去掉了一些xml标签，本身这些标签对于问题的表述没有较大影响。

## Link
Link为FAQ语料库的源网页地址链接，共有十一个链接。

## Content
Content为FAQ中的answers，是对应问题的解答模块。与title一致，去掉了一些xml标签，同时，还去掉的部分名词自带的网页链接（多是用于展开介绍），这一点值得注意。

## Process
Process将问题分为了几大类型，分别为：安装、数据处理、执行问题、编译、第三方框架迁移，调优，分布式配置，推理，特性咨询。其中“执行问题”，“第三方框架迁移”，“分布式配置”和“特性咨询”为新添加的process。其分类标准参照的是语料库的分类。

## Tag
Tag为FAQ所涉及到的领域，没有具体的分类标准。暂时将tag设置为以下类别：GPU、CuBLAS库、Linux、armmacOS、SDK版本、SciPy、whl包、protobuf、Ubuntu、MindInsight、MIndArmour、CPU、Windows、WSL、Ascend、Conda、Serving、gmp、cuda、MindIR、API、MindRecord、JupyterLab、Generator Dataset、MindData、pipeline、Datalouder、Dataset、eval、Model、SGD、loss、PyTorch、NLP、ModelZoo、HCLL、ModelArts、PyNative、Graph、TensorFlow、
C++、AIPP、OpenMPI、NCLL、RDMA、IB、RoCE、taichi、Caffe、NPU。其中所属有很多领域，包括系统，芯片，编程语言，部分库函数、环境等等。如果该问题不属于以上所有tag，则将其定义为’ ’，即空字符。

# Python源代码修改方法
####（1）如要增加语料库，需要在url，process和process1三个列表中添加元素，其中，url为语料库的网址链接，process为文章块的id，process1为定义的process属性。要注意的是，这三个列表需要对齐，否则会发生错误。
####（2）如在原有语料库的基础上增加QA，只需要重新运行该代码即可。
####（3）如要增加或者删减tag，只需要在tag列表中进行相应的添加或删减。在进行tag查找的时候，是不区分大小写的，所以进行添加时，对于大小写没有要求，最终输出是按照原文本的大小写进行输出的。
####（4）关于FAQ文本的要求，本代码可以区分中英文的冒号，同时，如果缺少‘A’字符，也是能够进行整理的。但是，如果出现‘Q’字符消失或者两个特征字符同时消失的情况，是不能进行整理的，需要注意。