FROM swr.cn-south-1.myhuaweicloud.com/mindspore/mindspore-cpu:1.8.0
RUN mkdir /code
WORKDIR /code
COPY src/qaRobot/requirements.txt /code/
RUN pip install -r requirements.txt
COPY src/qaRobot /code/
EXPOSE 8000
CMD ["python", "./manage.py", "runserver", "0.0.0.0:8000"]
