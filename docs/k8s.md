## 基于Minikube构建本地K8S集群

1. 在Windows上完成[WSL+Docker](https://zhuanlan.zhihu.com/p/500450853)安装和配置；

2. 安装[kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/);

3. 基于WSL+Ubuntu20.04 安装Minikube：

```
curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube-linux-amd64
sudo install minikube-linux-amd64 /usr/local/bin/minikube
```

执行`minikube start` 完成集群的创建，在创建的过程中，minikube会自动选择以Docker容器方式作为集群载体，所以在启动之后使用`docker ps`只能看到一个容器：

```
CONTAINER ID   IMAGE                    COMMAND                  CREATED        STATUS        PORTS
                    NAMES
699a71fee349   kicbase/stable:v0.0.30   "/usr/local/bin/entr…"   42 hours ago   Up 42 hours   127.0.0.1:49157->22/tcp, 127.0.0.1:49156->2376/tcp, 127.0.0.1:49155->5000/tcp, 127.0.0.1:49154->8443/tcp, 127.0.0.1:49153->32443/tcp   minikube
```

集群里面所有的容器都是以`docker in docker`的方式跑在这一个容器里面，所以如果执行`docker exec 699a71fee349 docker ps`可以看到一堆容器：

```
066d592cea3e   k8s.gcr.io/pause:3.6                   "/pause"                 42 hours ago        Up 42 hours                  k8s_POD_kube-proxy-rmm7s_kube-system_fcda4ef3-51ee-43a2-b227-23121eb05fb2_0
b00e5213c792   k8s.gcr.io/pause:3.6                   "/pause"                 42 hours ago        Up 42 hours                  k8s_POD_storage-provisioner_kube-system_6dc56ad3-203d-41b8-afc2-36e035b269f5_0
0a60ece008d5   99a3486be4f2                           "kube-scheduler --au…"   42 hours ago        Up 42 hours                  k8s_kube-scheduler_kube-scheduler-minikube_kube-system_be132fe5c6572cb34d93f5e05ce2a540_0
7df99575e6f0   f40be0088a83                           "kube-apiserver --ad…"   42 hours ago        Up 42 hours                  k8s_kube-apiserver_kube-apiserver-minikube_kube-system_cd6e47233d36a9715b0ab9632f871843_0
7a1d4fa569e4   25f8c7f3da61                           "etcd --advertise-cl…"   42 hours ago        Up 42 hours                  k8s_etcd_etcd-minikube_kube-system_9d3d310935e5fabe942511eec3e2cd0c_0
e90a8c076c20   b07520cd7ab7                           "kube-controller-man…"   42 hours ago        Up 42 hours                  k8s_kube-controller-manager_kube-controller-manager-minikube_kube-system_b965983e
...
```

执行`minikube dashboard`可以获得本地k8s集群的访问地址，在浏览器打开后就可以

```bash
$ minikube dashboard
🔌  Enabling dashboard ...
    ▪ Using image kubernetesui/dashboard:v2.3.1
    ▪ Using image kubernetesui/metrics-scraper:v1.0.7
🤔  Verifying dashboard health ...
🚀  Launching proxy ...
🤔  Verifying proxy health ...
🎉  Opening http://127.0.0.1:41919/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/ in your default browser...
👉  http://127.0.0.1:41919/api/v1/namespaces/kubernetes-dashboard/services/http:kubernetes-dashboard:/proxy/
```

4. 测试通过Minikube部署服务

比如部署Jenkins服务，执行如下命令:

```bash
kubectl create -n jenkins
kubectl apply -f infra/jenkins/jenkins.yaml -n jenkins
```

然后执行`kubectl get service -n jenkins`可以看到jenkins服务的状态：

```
iambowen@LAPTOP-ESJAVHR4:~/k8s/jenkins$ kubectl get service -n jenkins
NAME      TYPE           CLUSTER-IP      EXTERNAL-IP   PORT(S)          AGE
jenkins   LoadBalancer   10.101.12.241   127.0.0.1     8080:30415/TCP   42h
```

和WSL1不同，WSL2无法和宿主系统共享网络，它运行在hyper-v的VM中，所以无法直接在windows的浏览器中用`127.0.0.1:8080`来访问Jenkins服务，需要执行`minikube tunnel`，通过建立ssh 隧道的方式来让宿主访问。运行之后就可以在windows上的浏览器中打开并操作Jenkins了。
> 也可以用 `kubectl port-forward svc/jenkins 8080:8080 -n jenkins` 来实现相同的作用。

### 基于华为云创建和维护k8s集群

1. 在华为云创建CCE Turbo集群，并创建和纳管两个ECS节点。

2. 安装配置kubectl：

下载kubectl `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"` ，使用`sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl` 安装。

在CCE Turbo集群详情页面绑定公网IP，然后下载证书，按照指南创建 `$HOME/.kube`，并将证书添加到`$HOME/.kube/config`文件中。然后用`kubectl config use-context external`切换到互联网访问模式。

使用`kubectl get nodes`就可以看到节点的信息，验证配置无误：

```
$ kubectl get nodes
NAME           STATUS   ROLES    AGE    VERSION
192.168.0.34   Ready    <none>   5d1h   v1.19.10-r0-CCE21.12.1.B004
192.168.0.39   Ready    <none>   5d1h   v1.19.10-r0-CCE21.12.1.B004
```


### 参考资料

- https://www.jenkins.io/doc/book/installing/kubernetes/
- https://www.jenkins.io/doc/book/installing/kubernetes/#install-jenkins-with-yaml-files
- https://kubernetes.io/docs/tasks/access-application-cluster/ingress-minikube/  minikube enable ingress
- https://minikube.sigs.k8s.io/docs/start/  
- https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/
- https://github.com/argoproj/argo-workflows/releases/tag/v3.3.2  install argo cli
- https://zhuanlan.zhihu.com/p/153124468  如何在WSL下设置代理