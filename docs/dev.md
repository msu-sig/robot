## MindSpore 开发环境准备
项目目标为针对MindSpore官网主页的文档、FAQ等，构建知识机器人，可以让开发者通过机器人解决80%的问题，提升MindSpore文档易用性的体验。


## 环境准备

### 安装MindSpore以及相关的组件

当前MindSpore端到端工具链完整支持的系统为Linux，所以以下的安装指南会基于Ubuntu 18.04.

1. 安装MindSpore GPU版本：

MindSpore当前提供了Ubuntu的自动化安装脚本，参考[安装指南](https://gitee.com/mindspore/docs/blob/master/install/mindspore_gpu_install_pip.md)，安装Python 3.9 以及MindSpore 1.7.0版本，执行如下命令：

```bash
# 安装MindSpore 1.7.0，Python 3.9和CUDA 11.1。
wget https://gitee.com/mindspore/mindspore/raw/master/scripts/install/ubuntu-gpu-pip.sh
PYTHON_VERSION=3.9 MINDSPORE_VERSION=1.7.0 bash -i ./ubuntu-gpu-pip.sh
```

或者基于Conda，安装MindSpore GPU版本：

```bash
# 安装Python 3.9，CUDA 11.1以及最新版本的MindSpore
wget https://gitee.com/mindspore/mindspore/raw/master/scripts/install/ubuntu-gpu-conda.sh
PYTHON_VERSION=3.9 bash -i ./ubuntu-gpu-conda.sh
```

2. 安装调试调优工具MindInsight：

MindInsight是MindSpore的可视化调试调优工具。利用MindInsight，可以可视化地查看训练过程、优化模型性能、调试精度问题、解释推理结果。也是我们在开发过程中必不可少的工具。在命令行中执行`pip install mindinsight`即可完成MindInight的安装。

3. 安装可解释性工具XAI：

XAI是一个基于昇思MindSpore的可解释AI工具箱hao de 。当前深度学习模型多为黑盒模型，性能表现好但可解释性较差。XAI旨在为用户提供对模型决策的解释，帮助用户更好地理解模型、信任模型，以及当模型出现错误时有针对性地改进模型。除了提供多种解释方法，还提供了一套对解释方法效果评分的度量方法，从多种维度评估解释方法的效果，从而帮助用户比较和选择最适合于特定场景的解释方法。

```bash
curl -O https://ms-release.obs.cn-north-4.myhuaweicloud.com/1.5.0/Xai/any/mindspore_xai-1.5.0-py3-none-any.whl
pip install mindspore_xai-1.5.0-py3-none-any.whl
```

4. 安装可靠性、安全性工具MindArmour：

MindArmour是昇思MindSpore的一个子项目，为昇思MindSpore提供安全与隐私保护能力，主要包括对抗鲁棒性、模型安全测试、差分隐私训练、隐私泄露风险评估、数据漂移检测等技术。

```bash
version=1.7.0
pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/{version}/MindArmour/any/mindarmour-${version}-py3-none-any.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple
```

5. 安装MindSpore Serving：

MindSpore Serving是MindSpore的推理后端，用于在云侧部署推理服务，当前支持各类硬件平台（Nvidia GPU, Ascend系列芯片, CPU），在本项目中，Serving用于云侧推理服务的部署。

```bash
arch=x86_64
version=1.7.0
python_version=3.9.0
pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/${version}/Serving/${arch}/mindspore_serving-${version}-${python_version}-linux_${arch}.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple
```

6. 安装MindSpore Lite：

MindSpore Lite是端侧的推理框架，主要用于端侧设备的部署，在本项目中用于模型在IDE中的部署和推理。

```bash
curl -O https://ms-release.obs.cn-north-4.myhuaweicloud.com/1.7.0/MindSpore/lite/release/linux/x86_64/mindspore-lite-1.7.0-linux-x64.tar.gz
tar -xvzf mindspore-lite-1.7.0-linux-x64.tar.gz
```

### 安装IDE及MindSpore Dev Toolkit插件

1. 下载安装Pycharm IDE： Pycharm IDE是Jetbrains公司推出的针对Python开发的IDE，开发者可以选择[Pycharm社区版本](https://download.jetbrains.com/python/pycharm-community-2021.3.3.exe)进行安装。

2. 安装MindSpore Dev Tookkit插件：MindSpore Dev Toolkit是一款面向MindSpore开发者的IDE 开发插件，它提供了智能代码补全、算子互搜等能力，为MindSpore的开发者提供更好的编码体验。开发者在完成Pycharm的安装后，可以下载[Dev Toolkit插件](https://gitee.com/link?target=https%3A%2F%2Fms-release.obs.cn-north-4.myhuaweicloud.com%2F1.6.1%2FIdePlugin%2Fany%2FMindSpore_Dev_ToolKit-1.6.1.zip)，然后打开Pycharm，选择`File->Settings->Plugins->Install Plugin from Disk`，然后从本地磁盘加载刚下载的zip包，重启IDE之后就可以正常使用插件了。
