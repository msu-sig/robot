## 如何配置CI环境

### 本地启动Jenkins(Optional)

`infra` 目录提供了Jenkins的docker-compose文件，使用`docker-compose up -d`启动jenkins，通过`docker-compose logs`可以获得启动时的登录密码，浏览器中访问`localhost:8080`完成初始化即可。

### 基于K8S运行Jenkins

参考[基于Minikube构建本地K8S集群](../docs/k8s.md)，创建Jenkins服务，并在浏览器中访问Jenkins。初始admin用户密码可以通过`kubectl logs jenkins-789c9b6b84-8nwb7 -n jenkins`的日志中获取。

在Jenkins的插件管理中，安装`BlueOcean`和`Kubenetes` 插件。


### 创建流水线

参考代码仓中的Jenkinsfile，指定使用Kubernetes以及容器的模板：

```
pipeline {
    agent {
        kubernetes {
            // Rather than inline YAML, in a multibranch Pipeline you could use: yamlFile 'jenkins-pod.yaml'
            // Or, to avoid YAML:
            containerTemplate {
                name 'python'
                image 'bitnami/java:1.8'
                command 'sleep'
                args 'infinity'
            }
             // Can also wrap individual steps:
            // container('shell') {
            //     sh 'hostname'
            // }
            defaultContainer 'python'
        }
    }
   stages {
        stage('Code Check ') {
            steps("Code Check") {
                echo 'checking python code.'
                // sh 'bash scripts/codecheck.sh'
            }
        }
        stage('Unit Testing') {
            steps("Unit Testing") {
                echo "running unit tests"
                // sh 'bash scripts/unit.sh'
            }
        }
        stage('API Testing') {
            steps {
                echo 'API Testing....'
                // sh 'bash scripts/api.sh'
            }
        }
        stage('Continuous Training') {
            // when {
            //     changeset "data/*, src/train/*.py"
            // }
            steps("model training") {
                echo 'trigger continuous training jobs in argo'
                // sh 'bash scripts/data_process.sh'
            }
        }
        stage('Continuous Deployment') {
            //Decided by the training result
            steps("deploying") {
                echo 'deploy new version of model'
                // sh 'bash scripts/deploy.sh'
            }
        }
    }

}
```

### 参考资料
1. https://plugins.jenkins.io/kubernetes/#plugin-content-running-in-kubernetes  run with minikube
2. https://www.jenkins.io/doc/book/scaling/scaling-jenkins-on-kubernetes/ kubectl cluster-info



