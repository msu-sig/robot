# 特性开发任务列表

| 任务 | 活动类型 | 预期完成时间 |开发语言| 详情链接 | 任务状态 |任务认领人|导师|
| -------- | ---------------- | -------- | --- | --- |---|---|---|
| 易点通机器人-数据集准备（基于官网FAQ文档） | 实习 | 2022-5-30 | Python| [Link](https://gitee.com/mindspore/community/issues/I55IMN)|未认领|-|@zhangguoan.jack|
| 易点通机器人-数据集准备（基于官网API映射文档） | 活动 | 2022-5-30 | Python| [Link](https://gitee.com/mindspore/community/issues/I50YH6)|已认领|@lyofeng|@iambowen1984|
| 易点通机器人-多轮对话交互场景的模型实现 |活动 | 2022-6-30 |Python|[Link](https://gitee.com/mindspore/community/issues/I50YES)|已认领|@dede514|@iambowen1984|
| 易点通机器人-问题答案查询场景的模型实现 |活动 | 2022-6-30 |Python|[Link](https://gitee.com/mindspore/community/issues/I50TPP)|已认领|@pennhe|@iambowen1984|
| 易点通机器人-问答推荐场景的模型实现 |活动 | 2022-6-30 |Python|[Link](https://gitee.com/mindspore/community/issues/I55JNX)|已认领|@fenglovebei|@iambowen1984|