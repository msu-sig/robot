## 安装依赖

`pip install -r requirements.txt`

## 抓取opengauss官网文档

```
cd gaussdb_and_opengauss_dataset
python ./opengauss_dataset.py
```
结果会追加到data/data.csv文件，如果要单独生存csv，修改代码最后部分打开csv的路径

## 抓取CSDN社区高斯松鼠会的文章

```
cd gaussdb_and_opengauss_dataset
python ./csdn_GaussDB_dataset.py
```
结果会追加到data/data.csv文件，如果要单独生存csv，修改代码最后部分打开csv的路径
