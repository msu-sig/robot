## MindSpore 知识机器人

项目目标为针对MindSpore官网主页的文档、FAQ等，构建知识机器人，可以让开发者通过机器人解决80%的问题，提升MindSpore文档易用性的体验。


## 环境准备

### 安装MindSpore以及相关的组件

当前MindSpore端到端工具链完整支持的系统为Linux，所以以下的安装指南会基于Ubuntu 18.04.

1. 安装MindSpore GPU版本：

MindSpore当前提供了Ubuntu的自动化安装脚本，参考[安装指南](https://gitee.com/mindspore/docs/blob/master/install/mindspore_gpu_install_pip.md)，安装Python 3.9 以及MindSpore 1.7.0版本，执行如下命令：

```bash
# 安装MindSpore 1.7.0，Python 3.9和CUDA 11.1。
wget https://gitee.com/mindspore/mindspore/raw/master/scripts/install/ubuntu-gpu-pip.sh
PYTHON_VERSION=3.9 MINDSPORE_VERSION=1.7.0 bash -i ./ubuntu-gpu-pip.sh
```

或者基于Conda，安装MindSpore GPU版本：

```bash
# 安装Python 3.9，CUDA 11.1以及最新版本的MindSpore
wget https://gitee.com/mindspore/mindspore/raw/master/scripts/install/ubuntu-gpu-conda.sh
PYTHON_VERSION=3.9 bash -i ./ubuntu-gpu-conda.sh
```

2. 安装调试调优工具MindInsight：

MindInsight是MindSpore的可视化调试调优工具。利用MindInsight，可以可视化地查看训练过程、优化模型性能、调试精度问题、解释推理结果。也是我们在开发过程中必不可少的工具。在命令行中执行`pip install mindinsight`即可完成MindInight的安装。

3. 安装可解释性工具XAI：

XAI是一个基于昇思MindSpore的可解释AI工具箱hao de 。当前深度学习模型多为黑盒模型，性能表现好但可解释性较差。XAI旨在为用户提供对模型决策的解释，帮助用户更好地理解模型、信任模型，以及当模型出现错误时有针对性地改进模型。除了提供多种解释方法，还提供了一套对解释方法效果评分的度量方法，从多种维度评估解释方法的效果，从而帮助用户比较和选择最适合于特定场景的解释方法。

```bash
curl -O https://ms-release.obs.cn-north-4.myhuaweicloud.com/1.5.0/Xai/any/mindspore_xai-1.5.0-py3-none-any.whl
pip install mindspore_xai-1.5.0-py3-none-any.whl
```

4. 安装可靠性、安全性工具MindArmour：

MindArmour是昇思MindSpore的一个子项目，为昇思MindSpore提供安全与隐私保护能力，主要包括对抗鲁棒性、模型安全测试、差分隐私训练、隐私泄露风险评估、数据漂移检测等技术。

```bash
version=1.7.0
pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/{version}/MindArmour/any/mindarmour-${version}-py3-none-any.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple
```

5. 安装MindSpore Serving：

MindSpore Serving是MindSpore的推理后端，用于在云侧部署推理服务，当前支持各类硬件平台（Nvidia GPU, Ascend系列芯片, CPU），在本项目中，Serving用于云侧推理服务的部署。

```bash
arch=x86_64
version=1.7.0
python_version=3.9.0
pip install https://ms-release.obs.cn-north-4.myhuaweicloud.com/${version}/Serving/${arch}/mindspore_serving-${version}-${python_version}-linux_${arch}.whl --trusted-host ms-release.obs.cn-north-4.myhuaweicloud.com -i https://pypi.tuna.tsinghua.edu.cn/simple
```

6. 安装MindSpore Lite：

MindSpore Lite是端侧的推理框架，主要用于端侧设备的部署，在本项目中用于模型在IDE中的部署和推理。

```bash
curl -O https://ms-release.obs.cn-north-4.myhuaweicloud.com/1.7.0/MindSpore/lite/release/linux/x86_64/mindspore-lite-1.7.0-linux-x64.tar.gz
tar -xvzf mindspore-lite-1.7.0-linux-x64.tar.gz
```

### 安装IDE及MindSpore Dev Toolkit插件

1. 下载安装Pycharm IDE： Pycharm IDE是Jetbrains公司推出的针对Python开发的IDE，开发者可以选择[Pycharm社区版本](https://download.jetbrains.com/python/pycharm-community-2021.3.3.exe)进行安装。

2. 安装MindSpore Dev Tookkit插件：MindSpore Dev Toolkit是一款面向MindSpore开发者的IDE 开发插件，它提供了智能代码补全、算子互搜等能力，为MindSpore的开发者提供更好的编码体验。开发者在完成Pycharm的安装后，可以下载[Dev Toolkit插件](https://gitee.com/link?target=https%3A%2F%2Fms-release.obs.cn-north-4.myhuaweicloud.com%2F1.6.1%2FIdePlugin%2Fany%2FMindSpore_Dev_ToolKit-1.6.1.zip)，然后打开Pycharm，选择`File->Settings->Plugins->Install Plugin from Disk`，然后从本地磁盘加载刚下载的zip包，重启IDE之后就可以正常使用插件了。

### 如何在本地启动Jenkins

`infra` 目录提供了Jenkins的docker-compose文件，使用`docker-compose up -d`启动jenkins，通过`docker-compose logs`可以获得启动时的登录密码，浏览器中访问`localhost:8080`完成初始化即可。

### 如何创建和配置Kubernetes集群

1. 在华为云创建CCE Turbo集群，并创建和纳管两个ECS节点。

2. 安装配置kubectl：

下载kubectl `curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"` ，使用`sudo install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl` 安装。

在CCE Turbo集群详情页面绑定公网IP，然后下载证书，按照指南创建 `$HOME/.kube`，并将证书添加到`$HOME/.kube/config`文件中。然后用`kubectl config use-context external`切换到互联网访问模式。

使用`kubectl get nodes`就可以看到节点的信息，验证配置无误：

```
$ kubectl get nodes
NAME           STATUS   ROLES    AGE    VERSION
192.168.0.34   Ready    <none>   5d1h   v1.19.10-r0-CCE21.12.1.B004
192.168.0.39   Ready    <none>   5d1h   v1.19.10-r0-CCE21.12.1.B004
```

### 配置MLops流水线
社区机器人项目覆盖数据、特征、模型和应用开发等多个领域，为了促进项目中数据工程师、模型研究员以及应用开发人员等有效协作和工作，提升模型开发、部署和迭代效率，我们在项目中引入MLOps流水线简化ML任务的E2E生命周期。当前构建版本基于[MLOps-Tools](https://github.com/TECH4DX/MLops-Tools#readme)项目，相关配置说明可在项目仓库中查看。你可以参考[getting-started-guide.md](https://github.com/TECH4DX/MLops-Tools/blob/main/cd/examples/getting-started-guide.md)快速便捷地构建你的第一条流水线。


## 准备数据
data 路径下的data.csv为提取的官方文档中的数据。首先进入`src/model`目录，运行prepare_data.py生成预备数据。
* resource_sentence_encode.json是FAQ中问题的编码，key为问题，value为问题经过bert之后的编码。
* q_a.json 是所有FAQ的数据，key为问题，value为答案。

## 命令行测试
运行 `q_a.py`, 参数为问题，即可匹配到现有知识库中的答案。 
* 执行：`python q_a.py 改变第三方依赖库安装路径`
* 返回结果：第三方依赖库的包默认安装在build/mindspore/.mslib目录下，可以设置环境变量MSLIBS_CACHE_PATH来改变安装目录，比如 `export MSLIBS_CACHE_PATH = ~/.mslib`。

## 调用Restful接口访问
切换到`src/qaRobot`路径下，执行 `python manage.py runserver`
访问`http://127.0.0.1:8000/robot/api/?q=安装mindspore`即可返回答案,其中q参数为问题：
```json
{
    "question": "安装mindspore",
    "answer": "您可以从[MindSpore网站下载地址](https://www.mindspore.cn/versions)下载whl包，通过`pip install`命令进行安装。"
}
```


